#!/bin/bash
#
# This script will checkout the latest tools and
# build BDE UID with complete dependency chain.
#
# Author  : Hinko Kocevar
# Created : 25 Mar 2020

set -e
set -x

if [[ $# -lt 1 ]]; then
	echo "usage $0 uid"
	exit 1
fi

BUILD_UID="$1"
TOP_DIR="/data/bdee/ci"
TOOLS_DIR="$TOP_DIR/tools"
BUILD_DIR="$TOP_DIR/build"
DEST_DIR="/data/www/html/bdee/dist"

# get the latest bdee scripts
rm -fr $TOOLS_DIR
mkdir $TOOLS_DIR || exit 1
cd $TOOLS_DIR || exit 1
rm -fr bdee
git clone git@gitlab.esss.lu.se:beam-diagnostics/bde/bdee.git || exit 1
bash $TOOLS_DIR/bdee/bin/bdee.sh list

# invoke CI script
[[ -d $BUILD_DIR ]] || mkdir -p $BUILD_DIR

bash $TOOLS_DIR/bdee/bin/ci.sh $BUILD_UID $BUILD_DIR $DEST_DIR || exit 1

exit 0
